<?php

namespace Wm21w\Optima\Controller\Index;

class Checklogin extends \Magento\Framework\App\Action\Action
{
    public function execute()
    {
        require './vendor/autoload.php';

        $consumerKey = $_REQUEST['oauth_consumer_key'];
        $callback = $_REQUEST['callback_url'];

        /** Use $consumerKey to retrieve the following data in case it was stored in DB when received at "endpoint.php" */

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $this->_dbi = $resource->getConnection();

        $query = "SELECT * FROM wm21wcreditials";
        $entity = $this->_dbi->fetchRow($query);

        if ($consumerKey !== $entity['oauth_consumer_key']) {
            throw new \Exception("Consumer keys received on on different requests do not match.");
        }

        $consumerSecret = $entity['oauth_consumer_secret'];
        $magentoBaseUrl = rtrim($entity['store_base_url'], '/');
        $oauthVerifier = $entity['oauth_verifier'];

        define('TESTS_BASE_URL', $magentoBaseUrl);

        $credentials = new \OAuth\Common\Consumer\Credentials($consumerKey, $consumerSecret, $magentoBaseUrl);
        $oAuthClient = new OauthClient($credentials);
        $requestToken = $oAuthClient->requestRequestToken();
        $accessToken = $oAuthClient->requestAccessToken(
            $requestToken->getRequestToken(),
            $oauthVerifier,
            $requestToken->getRequestTokenSecret()
        );

        $query = "UPDATE  wm21wcreditials SET 
          request_token = '{$requestToken->getRequestToken()}',
          access_token = '{$accessToken->getAccessToken()}'
        ";
        $stmt = $this->_dbi->prepare($query);
        $result = $stmt->execute();

        header("location: $callback");
//        $this->_view->loadLayout();
//        $this->_view->getLayout()->initMessages();
//        $this->_view->renderLayout();
    }
}