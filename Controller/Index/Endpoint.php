<?php

namespace Wm21w\Optima\Controller\Index;

class Endpoint extends \Magento\Framework\App\Action\Action
{

    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $this->_dbi = $resource->getConnection();

        $query = "UPDATE  wm21wcreditials SET 
          oauth_consumer_key = '{$_POST['oauth_consumer_key']}',
          oauth_consumer_secret = '{$_POST['oauth_consumer_secret']}', 
          store_base_url = '{$_POST['store_base_url']}', 
          oauth_verifier = '{$_POST['oauth_verifier']}'
        ";
        $stmt = $this->_dbi->prepare($query);
        $result = $stmt->execute();

        // If this data is stored in the DB, oauth_consumer_key can be used as ID to retrieve this data later in "checklogin.php"
        // For simplicity of this sample, it is stored in session
        $_SESSION['oauth_consumer_key'] = $_POST['oauth_consumer_key'];

        $_SESSION['oauth_consumer_secret'] = $_POST['oauth_consumer_secret'];
        $_SESSION['store_base_url'] = $_POST['store_base_url'];
        $_SESSION['oauth_verifier'] = $_POST['oauth_verifier'];

//        file_put_contents('___creditials.txt', print_r([$_POST['oauth_consumer_key'], $_POST['oauth_consumer_secret'], $_POST['store_base_url'], $_POST['oauth_verifier']], true));

        header("HTTP/1.0 200 OK");
        echo "Response";

//        $this->_view->loadLayout();
//        $this->_view->getLayout()->initMessages();
//        $this->_view->renderLayout();
    }
}