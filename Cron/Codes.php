<?php

namespace Wm21w\Optima\Cron;

class Codes
{
    protected $logger;
    protected $_objectManager;
    protected $columns;
    protected $TableName = 'wm21wi';
    private $soapClient = null;
    private $client = null;
    private $wm21wiModel;
    private $filterManager;
    private $IsDifferential = false;
    private $test_mode = false;  // włancza tryb testowy import tylko 10 pierwszych produktów

    public function __construct(
        \Psr\Log\LoggerInterface $loggerInterface,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Zend\Soap\Client $soapClient,
        \Magento\Framework\Filter\FilterManager $filterManager
    )
    {
        $this->logger = $loggerInterface;
        $this->_objectManager = $objectManager;
        $this->wm21wiModel = $this->_objectManager->create('Wm21w\Optima\Model\Wm21wi');

        $this->TableName = $this->wm21wiModel->_applyTablePrefix("PFX_" . $this->TableName);


        $this->soapClient = $soapClient;
        $this->client = new  $this->soapClient($this->wm21wiModel->webServiceHost, ['soap_version' => SOAP_1_1]);

        $this->filterManager = $filterManager;
    }

    /**
     * test cron command line
     * php bin/magento cron:run --group="wm21w_optima_cron_group"
     * @return bool
     */
    public function execute()
    {

        $this->logger->info('start');

        // Lista typów atrybutów
//        $productAttrTypes = $this->wm21wiModel->getProductAttrTypes();
//        file_put_contents('__prod_attr_typses.txt', print_r($productAttrTypes, true));

        // jeśli pełny to uaktualnij kategorie
        if (!$this->IsDifferential) {
            $groups = $this->getProductGroups();
            $this->wm21wiModel->addCategories($groups);
        }

        if (!$this->wm21wiModel->checkIsAllDone()) {
            $productCodes = $this->getProductCodes();
            $this->setColumnsForProduct()
                ->_updateTmpProducts($productCodes);

        } else {
            $productCodes = $this->getProductCodes();
            $this->_saveTmpProductCodes($productCodes);
            $this->setColumnsForProduct()
                ->_updateTmpPrices()
                ->_updateTmpProducts($productCodes);
        }
        $this->logger->info('koniec');
        return true;
    }


    /**
     * Zakres kolumn przetwarzanych podczas importu danych z Optimy do tymczasowej tabeli wm21wi
     * @return $this
     */
    private function setColumnsForProduct()
    {
        $this->columns['txt'] = ['Images' => 'images', 'Groups' => 'select', 'Brand' => 'select', 'Description' => 'text', 'EAN' => 'text', 'Name' => 'text', 'ShortName' => 'text', 'Unit' => 'select', 'ParentProductCode' => 'text', 'Weight' => 'text'];
        $this->columns['json'] = ['Set', 'Substitute'];
        $this->columns['prices'] = ['DealersPrice', 'PromoPrice', 'RegularPrice', 'RetailPrice', 'SalePrice', 'Tax', 'Currency'];
        $this->columns['attr'] = ['Attributes' => [
            'Aktywny' => 'boolean',
            'NajlepiejOceniany' => 'select',
            'Nowosc' => 'select',
            'ProduktPolecany' => 'select',
            'ProduktzGazetki' => 'select',
            'Promocja' => 'select',
            'RekomendacjaSprzedawcy' => 'select',
            'SuperCena' => 'select',
            'SuperJakosc' => 'select',
            'Wyprzedaz' => 'select',
            'RodzajProduktu' => 'select',
            'RodzajWłosów' => 'multiselect',
            'Pojemność' => 'price',
            'OznaczenieKoloru' => 'select',
            'ProduktGrupujący' => 'text',
            'RodzajEfektu' => 'multiselect',

        ]];

        return $this;
    }

    /**
     * Czyszczenie tabeli tymczasowej a nastepnie zapis kodów produktów
     * @param $productCodes
     */
    private
    function _saveTmpProductCodes($productCodes)
    {
        // czyscimy tabele tymczasową wm21wi
        $query = "
			TRUNCATE TABLE {$this->TableName}
		";
        // lub archiwizujemy przetworzone dane
//        $query = "DELETE FROM {$this->TableName} WHERE `done` = {$this->wm21wiModel->tmpStstusDone}";
        if ($result = $this->wm21wiModel->_executeQuery($query)) {

            foreach ($productCodes as $a => $partial) {
                $query = "INSERT INTO {$this->TableName} (`code`,`done`) VALUES ";
                $query_values = [];
                foreach ($partial as $row) {
                    $query_values[] = "('" . $row . "'," . $this->wm21wiModel->tmpStstusEmpty . ")";
                }
                $query = $query . implode(',', $query_values);
                $this->wm21wiModel->_executeQuery($query);

                if ($this->test_mode) break; // tylko pierwsze 10 produktów
            }
        }
    }

    /**
     * Wprowadzenie danyuch z Optimy do tymczasowej tabeli Magento
     * @param $productCodes
     */
    private function _updateTmpProducts($productCodes)
    {
        if (!is_array($productCodes)) return $this;

        foreach ($productCodes as $a => $partial) {
            foreach ($partial as $code) {
                $tmpPorduct = $this->wm21wiModel->selectOneTmpProduct($code);
                if (isset($tmpPorduct['done']) && $tmpPorduct['done'] == 0) {

                    $query = "UPDATE {$this->TableName} SET `done` = {$this->wm21wiModel->tmpStstusProgress} WHERE `code` = '{$code}'";
                    $this->wm21wiModel->_executeQuery($query);
                    if ($product = $this->getProduct($code)) {

//                        file_put_contents($this->wm21wiModel->getDirectory('var') . '/tmp/' . $code . '____product.txt', print_r($product, true));

                        $query = "UPDATE {$this->TableName} SET ";
                        foreach ($this->columns['txt'] as $column => $type) {
                            if ($type == 'text') {
                                $attr = $this->prepareAttribiut($column, $product->{$column}, $type, $code);
                                $query .= '`' . strtolower($column) . '`' . " = '{$this->_parseValue($product->{$column})}',";
                            } else {
                                $attr = $this->prepareAttribiut($column, $product->{$column}, $type, $code);
                                $query .= '`' . strtolower($column) . '`' . " = '" . json_encode($attr) . "',";
                            }
                        }
                        foreach ($this->columns['json'] as $column) {
                            $query .= '`' . strtolower($column) . '`' . " = '" . json_encode($product->{$column}) . "',";
                        }

                        foreach ($this->columns['attr'] as $column => $attribiuts_types) {
                            $attr = $this->prepareAttribiut($column, $product->{$column}, $attribiuts_types);
                            $query .= '`' . strtolower($column) . '`' . " = '" . json_encode($attr) . "',";
                        }

                        $query .= "`done` = " . $this->wm21wiModel->tmpStstusRedy . ", created = '" . date('Y-m-d H:i:s') . "' ";
//                    $query = substr($query, 0, -1); // pozbywamy się ostatniego przecinka
                        $query .= " WHERE `code` = '{$code}' AND done != {$this->wm21wiModel->tmpStstusArch}";
                        $this->wm21wiModel->_executeQuery($query);
                    } else {
                        #TODO gdy po SKU nie można pobrać danych produktu
                        $query = "UPDATE {$this->TableName} SET `done` = {$this->wm21wiModel->tmpStstusErr} WHERE `code` = '{$code}'";
                        $this->wm21wiModel->_executeQuery($query);
                    }
                }
            }

            if ($this->test_mode) break; // do usunięcia
        }

        return $this;
    }

    private function _updateTmpPrices()
    {

        if ($prices = $this->getPrices()) {

//            file_put_contents('____prices.txt', print_r($prices, true));

            foreach ($prices as $a => $partial) {
                foreach ($partial as $price) {
                    $query = "UPDATE {$this->TableName} SET ";
                    foreach ($this->columns['prices'] as $column) {
                        $query .= '`' . strtolower($column) . '`' . " = '" . $price->{$column} . "',";
                    }
                    $query = substr($query, 0, -1); // pozbywamy się ostatniego przecinka
                    $query .= " WHERE `code` = '{$price->Code}'";
                    $this->wm21wiModel->_executeQuery($query);
                }
            }
        }

        return $this;
    }

    private function prepareAttribiut($attrColumn, $attrValue__, $type = 'text', $sku = null)
    {
        $attributs = [];
        switch ($attrColumn) {
            case "Images":
                $attrValue = $attrValue__;
                $attributs = $this->prepareImages($attrValue, $sku);
                break;
            case 'Brand':
                $attrCode = 'manufacturer';
                $attrValue = $attrValue__;
                $attributs = $this->attributeValidation($attrCode, $attrValue, $attrColumn, $type);
                break;

            case 'Unit':
                $attrCode = 'unit';
                $attrValue = $attrValue__;
                $attributs = $this->attributeValidation($attrCode, $attrValue, $attrColumn, $type);
                break;

            case 'Groups':
                $attrCode = 'groups';
                $attrValue = $attrValue__;
                $attributs = $this->mapCategoriesAndGroups($attrValue);
                break;

            case 'Attributes':

                if (is_array($attrValue__->ProductAttribute)) {
                    $attributs = [];
                    foreach ($attrValue__->ProductAttribute as $attribut) {

                        $attr_type = $this->checkAttributType($attribut->Name, $type);

                        $attrCode = $this->filterManager->translitUrl($attribut->Name);
                        $attrValue = $attribut->Value;

                        switch ($attr_type) {
                            case 'boolean':

                                switch ($attrCode) {
                                    case 'aktywny':
                                        $attrValue = ($attrValue == 'TAK') ? 1 : 0;
                                        $attributs[] = ['attribute_code' => $attrCode, 'value' => $attrValue];
                                        break;

//                                    case 'produktpolecany':
//                                        $attrValue = ($attrValue == 'TAK') ? 1 : 0;
//                                        $attributs[] = ['attribute_code' => 'sm_featured', 'value' => $attrValue];
//                                        break;

//                                    case 'promocja':
//                                        $attrValue_ = ($attrValue == 'TAK') ? 1 : 0;
//                                        $attributs[] = ['attribute_code' => 'promocja', 'value' => $attrValue_];
//                                        $this->magentoAttributes('promocja', $attrValue, 'Promocja Home', $attr_type);
//
//                                        $this->magentoAttributes('promocja_strona', $attrValue, $attribut->Name, $attr_type);
//                                        $allAttribiutOptions = $this->wm21wiModel->getAttribiutOptions('promocja_strona');
//                                        if ($attribiutOptionId = $this->wm21wiModel->getAttributOptionId($attrValue, $allAttribiutOptions)) {
//                                            $attributs[] = ['attribute_code' => 'promocja_strona', 'value' => $attribiutOptionId];
//                                        }
//                                        break;
//
//                                    case 'wyprzedaz':
//                                        $attrValue_ = ($attrValue == 'TAK') ? 1 : 0;
//                                        $attributs[] = ['attribute_code' => 'wyprzedaz', 'value' => $attrValue_];
//                                        $this->magentoAttributes('wyprzedaz', $attrValue, 'Wyprzedaż Home', $attr_type);
//
//                                        $this->magentoAttributes('wyprzedaz_strona', $attrValue, 'Wyprzedaż', $attr_type);
//                                        $allAttribiutOptions = $this->wm21wiModel->getAttribiutOptions('wyprzedaz_strona');
//                                        if ($attribiutOptionId = $this->wm21wiModel->getAttributOptionId($attrValue, $allAttribiutOptions)) {
//                                            $attributs[] = ['attribute_code' => 'wyprzedaz_strona', 'value' => $attribiutOptionId];
//                                        }
//                                        break;
//
//                                    case 'nowosc':
//                                        $attrValue_ = ($attrValue == 'TAK') ? 1 : 0;
//                                        $attributs[] = ['attribute_code' => 'nowosc', 'value' => $attrValue_];
//                                        $this->magentoAttributes('nowosc', $attrValue, 'Nowość Home', $attr_type);
//
//                                        $this->magentoAttributes('nowosc_strona', $attrValue, 'Nowość', $attr_type);
//                                        $allAttribiutOptions = $this->wm21wiModel->getAttribiutOptions('nowosc_strona');
//                                        if ($attribiutOptionId = $this->wm21wiModel->getAttributOptionId($attrValue, $allAttribiutOptions)) {
//                                            $attributs[] = ['attribute_code' => 'nowosc_strona', 'value' => $attribiutOptionId];
//                                        }
//                                        break;

                                    default:
                                        $attrValue = ($attrValue == 'TAK') ? 1 : 0;
                                        $attributs[] = ['attribute_code' => $attrCode, 'value' => $attrValue];
                                        $this->magentoAttributes($attrCode, $attrValue, $attribut->Name, $attr_type);
                                        break;
                                }
                                break;

                            default:
                                switch ($attrCode) {
                                    case 'pojemnosc':
                                        $poj = explode(' ', $attrValue);
                                        $poj_val = (isset($poj[0])) ? $poj[0] : 0;
                                        $this->magentoAttributes($attrCode, $poj_val, $attribut->Name, $attr_type);
                                        $attributs[] = ['attribute_code' => $attrCode, 'value' => $poj_val];
                                        $this->magentoAttributes('pojemnoscopis', $attrValue, $attribut->Name, 'text');
                                        $attributs[] = ['attribute_code' => 'pojemnoscopis', 'value' => $attrValue];
                                        break;

//                                    case 'oznaczeniekoloru':
//                                        $attributs[] = ['attribute_code' => 'nazwaoznaczeniekoloru', 'value' => $attrValue];
//                                        $this->magentoAttributes($attrCode, $attrValue, $attribut->Name, $attr_type);
//
//                                        $allAttribiutOptions = $this->wm21wiModel->getAttribiutOptions($attrCode);
//                                        if ($attribiutOptionId = $this->wm21wiModel->getAttributOptionId($attrValue, $allAttribiutOptions)) {
//                                            $attributs[] = ['attribute_code' => $attrCode, 'value' => $attribiutOptionId];
//                                        }
//                                        break;


                                    default:
                                        $this->magentoAttributes($attrCode, $attrValue, $attribut->Name, $attr_type);

                                        $allAttribiutOptions = $this->wm21wiModel->getAttribiutOptions($attrCode);
                                        if ($attribiutOptionId = $this->wm21wiModel->getAttributOptionId($attrValue, $allAttribiutOptions)) {
                                            $attributs[] = ['attribute_code' => $attrCode, 'value' => $attribiutOptionId];
                                        }
                                        break;
                                }
                                break;
                        }
                    }
                }
                break;

            default:
                $attrValue = $attrValue__;
                $attrCode = $this->filterManager->translitUrl($attrColumn);
                $attributs = $this->attributeValidation($attrCode, $attrValue, $attrColumn, $type);
                break;
        }
        return $attributs;
    }


    private function prepareImages($attrValue, $sku)
    {

        $directory = $this->wm21wiModel->getDirectory('var') . '/tmp';
        if (chmod($directory, 0777)) {
            // more code
            chmod($directory, 0755);
        }
        $gallery = [];
        $i = 0;

        if (isset($attrValue->Image)) {
            $image = $attrValue->Image;
            if (isset($image->Data)) {
                file_put_contents($directory . '/' . $image->Name, $image->Data);
                $gallery[] = $image->Name;
            } else if (is_array($image)) {
                foreach ($image as $key => $images) {
                    file_put_contents($directory . '/' . $images->Name, $images->Data);
                    $gallery[] = $images->Name;
                    $i++;
                }
            }
        }
        return $gallery;
    }

    /**
     * Dodaje atrybutu oraz opcje w Magento
     *
     * @param $attrCode
     * @param $attrValue
     * @param $attributName
     * @param $attr_type
     */
    private
    function magentoAttributes($attrCode, $attrValue, $attributName, $attr_type)
    {
        if ($this->wm21wiModel->checkIsAttributeExistInMagento($attrCode)) {
            switch ($attr_type) {
                case 'select':
                    $allAttribiutOptions = $this->wm21wiModel->getAttribiutOptions($attrCode);
                    if ($attribiutOptionId = $this->wm21wiModel->getAttributOptionId($attrValue, $allAttribiutOptions)) {

                    } else {
                        //dodać tylko opcje do atrybutu
                        $respons = $this->wm21wiModel->setAttribiutOptions($attrCode, $attrValue);
                    }
                    break;

                case 'multiselect':
                    $allAttribiutOptions = $this->wm21wiModel->getAttribiutOptions($attrCode);
                    if ($attribiutOptionId = $this->wm21wiModel->getAttributOptionId($attrValue, $allAttribiutOptions)) {

                    } else {
                        //dodać tylko opcje do atrybutu
                        $respons = $this->wm21wiModel->setAttribiutOptions($attrCode, $attrValue);
                    }
                    break;
                default:
                    //add niew attribute and option
                    $respons = $this->wm21wiModel->setAttribiut($attrCode, $attributName, $attrValue, $attr_type);

                    break;
            }
        } else {
            //add niew attribute and option
            $respons = $this->wm21wiModel->setAttribiut($attrCode, $attributName, $attrValue, $attr_type);
        }
    }

    private
    function mapCategoriesAndGroups($groups)
    {
        $categories = [];
        if (is_array($groups->Id)) {
            foreach ($groups->Id as $group_id) {
                if ($category_id = $this->wm21wiModel->getCategoryIdByGroupId($group_id)) {
                    $categories[] = $category_id;
                }
            }
        }
        return $categories;
    }

    /**
     * Sprawdzamy czy atrybut ma w Nazwie słowo Lista jeśli tak to ustawiamy go tym Select
     *
     * @param $attributName
     * @param $type
     * @return string
     */
    private
    function checkAttributType($attributName, $type)
    {

        if (preg_match("/Lista/i", $attributName)) {
            $attr_type = 'select';
        } else {
            $attr_type = (array_key_exists($attributName, $type)) ? $type[$attributName] : 'text';
        }
        return $attr_type;
    }


    private
    function attributeValidation($attrCode, $attrValue, $attrColumn, $type)
    {
        $attributs = [];
        if (!empty($attrValue)) {
            if ($this->wm21wiModel->checkIsAttributeExistInMagento($attrCode)) {
                $allAttribiutOptions = $this->wm21wiModel->getAttribiutOptions($attrCode);
                if ($attribiutOptionId = $this->wm21wiModel->getAttributOptionId($attrValue, $allAttribiutOptions)) {
                    $attributs[] = ['attribute_code' => $attrCode, 'value' => $attribiutOptionId];
                } else {
                    $respons = $this->wm21wiModel->setAttribiutOptions($attrCode, $attrValue);
                    $allAttribiutOptions = $this->wm21wiModel->getAttribiutOptions($attrCode);
                    if ($attribiutOptionId = $this->wm21wiModel->getAttributOptionId($attrValue, $allAttribiutOptions)) {
                        $attributs[] = ['attribute_code' => $attrCode, 'value' => $attribiutOptionId];
                    }

                }
            } else {
                $respons = $this->wm21wiModel->setAttribiut($attrCode, $attrColumn, $attrValue, $type);
                $allAttribiutOptions = $this->wm21wiModel->getAttribiutOptions($attrCode);
                if ($attribiutOptionId = $this->wm21wiModel->getAttributOptionId($attrValue, $allAttribiutOptions)) {
                    $attributs[] = ['attribute_code' => $attrCode, 'value' => $attribiutOptionId];
                }
            }
        }
        return $attributs;
    }

    private
    function _parseValue($val)
    {
        return str_replace("`", "\`", str_replace("'", "\'", $val));
    }

    private
    function getProductGroups()
    {
        try {
            $results = $this->client->GetProductGroups([
                'Authentication' => [
                    'Password' => $this->wm21wiModel->webServicePassword,
                    'Username' => $this->wm21wiModel->webServiceUsername,
                    'ShopId' => $this->wm21wiModel->webServiceShopId
                ]
            ]);
            if ($this->_checkRespond($results)) {
                return $results;
            } else {
                return false;
            }
        } catch (Exception $e) {
            $this->logger->debug($e->getMessage() . print_r($e, true));
        }
    }

    /**
     * Odczyt list kodów Pełnej lub różnicowo
     * @param bool $IsDifferential
     * @return array|bool
     */
    private
    function getProductCodes()
    {
        try {
            $results = $this->client->GetProductCodes([
                'Authentication' => [
                    'Password' => $this->wm21wiModel->webServicePassword,
                    'Username' => $this->wm21wiModel->webServiceUsername,
                    'ShopId' => $this->wm21wiModel->webServiceShopId
                ],
                'IsDifferential' => $this->IsDifferential
            ]);
            if ($this->_checkRespond($results)) {
                $codes_ = array_chunk($results->ProductCodes->string, 10);

                $this->client->Confirm([
                    'Authentication' => [
                        'Password' => $this->wm21wiModel->webServicePassword,
                        'Username' => $this->wm21wiModel->webServiceUsername,
                        'ShopId' => $this->wm21wiModel->webServiceShopId
                    ],
                    'ConfirmationType' => 'ProductsCodes'
                ]);

                return $codes_;
            } else {
                return false;
            }
        } catch (Exception $e) {
            $this->logger->debug($e->getMessage() . print_r($e, true));
        }
    }

    private
    function getProduct($code = null)
    {
        try {
            $results = $this->client->GetProduct([
                'Authentication' => [
                    'Password' => $this->wm21wiModel->webServicePassword,
                    'Username' => $this->wm21wiModel->webServiceUsername,
                    'ShopId' => $this->wm21wiModel->webServiceShopId
                ],
                'ProductCode' => $code
            ]);
            if ($this->_checkRespond($results)) {
                return $results->Product;
            } else {
                return false;
            }
        } catch (Exception $e) {
            $this->logger->debug($e->getMessage() . print_r($e, true));
        }
    }

    private
    function getPrices()
    {
        try {
            $results = $this->client->GetPrices([
                'Authentication' => [
                    'Password' => $this->wm21wiModel->webServicePassword,
                    'Username' => $this->wm21wiModel->webServiceUsername,
                    'ShopId' => $this->wm21wiModel->webServiceShopId
                ],
                'IsDifferential' => $this->IsDifferential
            ]);
            if ($this->_checkRespond($results)) {
                $codes_ = array_chunk($results->Prices->Price, 10);
                return $codes_;
            } else {
                return false;
            }
        } catch (Exception $e) {
            $this->logger->debug($e->getMessage() . print_r($e, true));
        }
    }

    private
    function _checkRespond($results)
    {
        if (!$results->ErrorMessage
            && $results->LoginResult == 'OK'
        ) {
            return true;
        } else {
            return false;
        }
    }
}