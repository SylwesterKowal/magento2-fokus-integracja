<?php

namespace Wm21w\Optima\Cron;

class Pricesdiff
{
    protected $logger;
    private $soapClient = null;
    private $client = null;
    private $wm21wiModel;
    private $IsDifferential = true;

    public function __construct(
        \Psr\Log\LoggerInterface $loggerInterface,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Zend\Soap\Client $soapClient
    )
    {
        $this->logger = $loggerInterface;
        $this->_objectManager = $objectManager;
        $this->wm21wiModel = $this->_objectManager->create('Wm21w\Optima\Model\Wm21wi');
        $this->soapClient = $soapClient;
        $this->client = new  $this->soapClient($this->wm21wiModel->webServiceHost, ['soap_version' => SOAP_1_1]);

    }

    public function execute()
    {

        if ($prices = $this->getPrices()) {

            foreach ($prices as $a => $partial) {
                foreach ($partial as $price) {

                    $mode = $this->wm21wiModel->checkIsProductExistInMagento($price->Code);

                    if ($mode == 'update') {
                        $tmpProduct = $this->prepareTmpProductToSave($price);
                        $this->wm21wiModel->updateProduct($tmpProduct);
                    }
                }
            }
        }
        return true;
    }

    private function prepareTmpProductToSave($price)
    {


        $customAttr = [
            ['attribute_code' => 'special_price', 'value' => $this->setSpecialPrice($price)],
            ['attribute_code' => 'msrp', 'value' => $price->RegularPrice],
        ];

        if (($price->DealersPrice > 0)) {
            $dealersprice[] = [
                "customer_group_id" => 2,
                "qty" => 1,
                "value" => $price->DealersPrice
            ];
        } else {
            $dealersprice = null;
        }

        return
            [
                'product' =>
                    [
                        'sku' => $price->Code,
                        'price' => $price->RetailPrice,
                        "tier_prices" => $dealersprice,
                        'custom_attributes' => $customAttr
                    ]
            ];
    }


    private
    function getPrices()
    {
        try {
            $results = $this->client->GetPrices([
                'Authentication' => [
                    'Password' => $this->wm21wiModel->webServicePassword,
                    'Username' => $this->wm21wiModel->webServiceUsername,
                    'ShopId' => $this->wm21wiModel->webServiceShopId
                ],
                'IsDifferential' => $this->IsDifferential
            ]);
            if ($this->_checkRespond($results)) {
                $codes_ = array_chunk($results->Prices->Price, 100);

                $this->client->Confirm([
                    'Authentication' => [
                        'Password' => $this->wm21wiModel->webServicePassword,
                        'Username' => $this->wm21wiModel->webServiceUsername,
                        'ShopId' => $this->wm21wiModel->webServiceShopId
                    ],
                    'ConfirmationType' => 'Prices'
                ]);

                return $codes_;
            } else {
                return false;
            }
        } catch (Exception $e) {
            $this->logger->debug($e->getMessage() . print_r($e, true));
        }
    }

    private function setSpecialPrice($price)
    {
        $promocja = $this->wm21wiModel->getAttribiutValue('promocja', $price->Code, 'catalog_product_entity_int'); // Yes|No
        $wyprzedaz = $this->wm21wiModel->getAttribiutValue('wyprzedaz', $price->Code, 'catalog_product_entity_int'); // Yes|No

        if ($promocja == 'TAK' && $price->PromoPrice > 0) {
            return $price->PromoPrice;
        }
        if ($wyprzedaz == 'TAK' && $price->SalePrice > 0) {
            return $price->SalePrice;
        }
        return null;
    }


    private
    function _checkRespond($results)
    {
        if (!$results->ErrorMessage
            && $results->LoginResult == 'OK'
        ) {
            return true;
        } else {
            return false;
        }
    }

}