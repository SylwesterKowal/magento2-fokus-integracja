<?php

namespace Wm21w\Optima\Cron;

class Productsdiff
{
    protected $logger;
    protected $wm21wiModel;
    protected $filterManager;
    protected $_objectManager;
    protected $_numberProductToAdd = 30;

    public function __construct(
        \Psr\Log\LoggerInterface $loggerInterface,
        \Magento\Framework\ObjectManagerInterface $objectManager
    )
    {
        $this->logger = $loggerInterface;
        $this->_objectManager = $objectManager;
        $this->wm21wiModel = $this->_objectManager->create('Wm21w\Optima\Model\Wm21wi');
        $this->filterManager = $objectManager->get('Magento\Framework\Filter\FilterManager');

    }

    public function execute()
    {

        $this->updateProducts();
        $this->updateProducts();

        return true;
    }


    /**
     * Dodaje lub uaktualnia produkt w oparciu o dane z tabeli wm21wi
     */
    private function updateProducts()
    {
        if ($this->wm21wiModel->checkIsProductsRedy()) {
            $query = "SELECT `code`,`parentproductcode`  FROM {$this->wm21wiModel->TableName} WHERE `done` = :done_status LIMIT {$this->_numberProductToAdd}";
            $codes = $this->wm21wiModel->_dbi->fetchAssoc($query, array('done_status' => $this->wm21wiModel->tmpStstusRedy));


            // ustaw jako realizowane
            foreach ($codes as $row) {
                $query = "UPDATE {$this->wm21wiModel->TableName} SET `done` = {$this->wm21wiModel->tmpStstusProgressData} WHERE `done` = '{$this->wm21wiModel->tmpStstusRedy}'";
                $this->wm21wiModel->_executeQuery($query);
            }

            foreach ($codes as $row) {
                $mode = $this->wm21wiModel->checkIsProductExistInMagento($row['code']);

                if ($tmpProduct = $this->getOneTmpProductData($row['code'], $mode)) {
                    switch ($mode) {
                        case 'update':
//                            $this->wm21wiModel->updateProduct($tmpProduct);
                            break;
                        case 'add':
                            $this->wm21wiModel->addProduct($tmpProduct);
                            break;
                    }
                    if (!empty($row['parentproductcode'])) {
                        $mode = $this->wm21wiModel->checkIsProductExistInMagento($row['parentproductcode']);

                        switch ($mode) {
                            case 'update':
                                if ($tmpConfigProduct = $this->getConfigProductData($row['parentproductcode'], $mode, $row['code'])) {
                                    file_put_contents($this->wm21wiModel->getDirectory('var') . '/tmp/___config_save_' . $row['parentproductcode'] . '_product.txt', print_r($tmpConfigProduct, true));
                                    $this->wm21wiModel->updateProduct($tmpConfigProduct);
                                }
                                break;

                        }

                    }

                    $this->wm21wiModel->setDone($row['code']);
                }
            }
        }
    }


    /**
     * Pobiera wszystkie dane na temat jednego produktu Tmp
     * @param $sku
     * @return bool
     */
    private function getOneTmpProductData($sku, $mode = 'add')
    {
        $entity = $this->wm21wiModel->selectOneTmpProduct($sku);
        if (empty($entity) == true) {
            return false;
        } else {
            return $this->prepareTmpProductToSave($entity, $mode);
        }
    }

    private function getConfigProductData($sku, $mode = 'update', $simple_sku)
    {
        $jsProduct = $this->wm21wiModel->getProduct($sku);

        if (empty($jsProduct) == true) {
            return false;
        } else {
            $simple = $this->wm21wiModel->selectOneTmpProduct($simple_sku);
            return $this->configurableProduct($jsProduct, $simple);
        }
    }


    /**
     * Jeśli produkt prosty posiada parentproductcode na jego podstawie odczytywany jest rekod produktu config.
     * Najpierw na bazie funkcji simple product tworzony jest obiekt produktu do którego są dodawane powiązania
     * do produktów prostych w produkcie configurowalnym.
     *
     * Dodajemy do produktu konfigurowalnego produkt prosty na którym stoii skrypt.
     *
     * @param $config
     * @param $simple
     * @return mixed
     */
    private function configurableProduct($config, $simple)
    {
        $simple_entity_id = $this->wm21wiModel->checkIsProductExistInMagento($simple['code'], true);
        $config['type_id'] = 'configurable';
        $config['price'] = 0;


        // produkty
        if (isset($config['extension_attributes']['configurable_product_links']) && is_array($config['extension_attributes']['configurable_product_links'])) {
            $isLinked = false; // sprawdzamy czy simple produkt jest już przypisany do configurable
            foreach ($config['extension_attributes']['configurable_product_links'] as $cId => $configurable_product_link) {
                if ($config['id'] == $configurable_product_link) {
                    $isLinked = true;
                }
            }
            if (!$isLinked) {
                $config['extension_attributes']["configurable_product_links"][] = $simple_entity_id;
            }
        } else {
            $config['extension_attributes']["configurable_product_links"][] = $simple_entity_id;
        }
        //opcje
        if (isset($config['extension_attributes']['configurable_product_options']) && is_array($config['extension_attributes']['configurable_product_options'])) {
            foreach ($config['extension_attributes']['configurable_product_options'] as $cId => $configurable_product_option) {
                $config['extension_attributes']['configurable_product_options'][$cId]['values'] = $this->configurableCheckOptions($simple, $configurable_product_option['values']);
            }
        } else {
            $config = $this->addNewOptions($config, $simple);
        }

        return ['product' => $config];
    }


    private function addNewOptions($config, $simple)
    {
        $config['extension_attributes']["configurable_product_options"][] =
            [
                "attribute_id" => $this->wm21wiModel->_getAttributeId("oznaczeniekoloru"),
                "label" => "Oznaczenie Koloru",
                "position" => 0,
                "is_use_default" => true,
                "values" => $this->configurableCheckOptions($simple, false),
                "product_id" => $config['id']
            ];
        return $config;
    }


    private
    function configurableCheckOptions($simple, $configurable_product_option_values)
    {
        $attributes = json_decode($simple['attributes'], true);
        if (is_array($attributes)) {
            if (is_array($configurable_product_option_values)) {
                foreach ($configurable_product_option_values as $opKey => $option_id) {

                    foreach ($attributes as $attr) {
                        if ($attr['attribute_code'] == 'oznaczeniekoloru') {
                            $find = false;
                            if ($option_id == $attr['value']) {
                                $find = true;
                            }
                            if (!$find) {
                                $configurable_product_option_values[] = [
                                    "value_index" => $attr['value']
                                ];
                            }
                        }
                    }
                }
            } else {
                foreach ($attributes as $attr) {
                    if ($attr['attribute_code'] == 'oznaczeniekoloru') {
                        $configurable_product_option_values[] = [
                            "value_index" => $attr['value']
                        ];
                    }
                }
            }
        }
        return $configurable_product_option_values;


    }


    private
    function prepareTmpProductToSave($product, $mode, $type = 'simple')
    {
        $attributes = json_decode($product['attributes'], true);

        $poje = $this->getPojemnosc($product);
        $pojemnoscOpis = (isset($poje['pojemnoscopis'])) ? $poje['pojemnoscopis'] : '';
        $pojemnosc = (isset($poje['pojemnosc'])) ? $poje['pojemnosc'] : '';
        $customAttr = [
            ['attribute_code' => 'pojemnosc', 'value' => $pojemnosc],
            ['attribute_code' => 'pojemnoscopis', 'value' => $pojemnoscOpis],
            ['attribute_code' => 'ean', 'value' => $product['ean']],
            ['attribute_code' => 'special_price', 'value' => $this->setSpecialPrice($product)],
            ['attribute_code' => 'msrp', 'value' => $product['regularprice']],
            ['attribute_code' => 'description', 'value' => $product['description']],
            ['attribute_code' => 'short_description', 'value' => $product['shortname']],
            ['attribute_code' => 'shortname', 'value' => $product['shortname']],
            ['attribute_code' => 'category_ids', 'value' => json_decode($product['groups'], true)]

        ];

        $extAttr = [
//            "website_ids" => [
//                0
//            ],
//            "stock_item" => [
//                "qty" => 100,
//                "is_in_stock" => true
//            ]
        ];

        $oznaczeniekoloru = '';
        if (is_array($attributes)) {
            foreach ($attributes as $attr) {
                if ($attr['attribute_code'] == 'aktywny') {
                    $status = $attr['value'];
                }
                if ($attr['attribute_code'] == 'nazwaoznaczeniekoloru') {
                    $oznaczeniekoloru = $attr['value'];
                }
                if (is_array($attr) && !empty($attr)) $customAttr[] = $attr;
            }
        }

        if ($mode == 'add') {
            $customAttr[] = ['attribute_code' => 'url_key', 'value' => $attCode = $this->filterManager->translitUrl($product['name'] . ' ' . $product['ean'] . ' ' . $oznaczeniekoloru)];
        }

        $brand = json_decode($product['brand'], true);
        if (isset($brand[0]) && !empty($brand[0])) $customAttr[] = $brand[0];

        $unit = json_decode($product['unit'], true);
        if (isset($unit[0]) && !empty($unit[0])) $customAttr[] = $unit[0];

        $related_json = json_decode($product['substitute'], true);
        $related_arry = [];
        if (isset($related_json['Code'])) {
//            file_put_contents($this->wm21wiModel->getDirectory('var') . '/tmp/___linked___' . $product['code'] . '_product.txt', print_r($related_json, true));
            if (!is_array($related_json['Code'])) {
                $related_arry[] = [
                    "sku" => $product['code'],
                    "link_type" => "related",
                    "linked_product_sku" => $related_json['Code'],
                    "linked_product_type" => "simple",
                    "position" => 0
                ];
            } else {
                foreach ($related_json['Code'] as $related) {
                    $related_arry[] = [
                        "sku" => $product['code'],
                        "link_type" => "related",
                        "linked_product_sku" => $related['Code'],
                        "linked_product_type" => "simple",
                        "position" => 0
                    ];
                }
            }
        }

        if ($product['dealersprice'] > 0) {
            $dealersprice[] = [
                "customer_group_id" => 2,
                "qty" => 1,
                "value" => $product['dealersprice']
            ];
        } else {
            $dealersprice = null;
        }

        return
            [
                'product' =>
                    [
                        'sku' => $product['code'],
                        'name' => $product['name'] . ' ' . $oznaczeniekoloru, // EAN dodajemy ze względu na unikalność nazwy produktu
                        'visibility' => 4, /*'catalog',*/
//                        'type_id' => $type,
                        'price' => $product['retailprice'],
                        'status' => $status,
                        'attribute_set_id' => 4,
                        'weight' => $product['weight'],
//                        'extension_attributes' => $extAttr,
                        "tier_prices" => $dealersprice,
                        "product_links" => $related_arry,
                        'custom_attributes' => $customAttr
                    ],
                // dane dla addMedia - dane z poza struktury addproduct
                'images' => $product['images']
            ];
    }

    private
    function setSpecialPrice($prooduct)
    {
        $attr = json_decode($prooduct['attributes']);
        $specPrice = $this->attributesValues($attr);
        if ($specPrice['promocja'] == 'TAK' && $prooduct['promoprice'] > 0) {
            return $prooduct['promoprice'];
        }
        if ($specPrice['wyprzedaz'] == 'TAK' && $prooduct['saleprice'] > 0) {
            return $prooduct['saleprice'];
        }

        return null;
    }

    private
    function attributesValues($attrs)
    {
        $specPrice = [];
        foreach ($attrs as $key => $attr) {
            switch ($attr->attribute_code) {
                case 'promocja':
                    $specPrice['promocja'] = $attr->value;
                    break;
                case 'wyprzedaz':
                    $specPrice['wyprzedaz'] = $attr->value;
                    break;
                case 'pojemnosc':
                    $specPrice['pojemnosc'] = $attr->value;
                    break;
                case 'pojemnoscopis':
                    $specPrice['pojemnoscopis'] = $attr->value;
                    break;
            }
        }
        return $specPrice;
    }


    private
    function getPojemnosc($prooduct)
    {
        $attr = json_decode($prooduct['attributes']);
        $attrVal = $this->attributesValues($attr);
        $value = (isset($attrVal['pojemnosc'])) ? $attrVal['pojemnosc'] : 0;
        $opis = (isset($attrVal['pojemnoscopis'])) ? $attrVal['pojemnoscopis'] : '';
        return ['pojemnosc' => $value, 'pojemnoscopis' => $opis];
    }
}