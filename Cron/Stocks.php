<?php

namespace Wm21w\Optima\Cron;

class Stocks
{
    protected $logger;
    private $soapClient = null;
    private $client = null;
    private $wm21wiModel;
    private $IsDifferential = false;

    public function __construct(
        \Psr\Log\LoggerInterface $loggerInterface,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Zend\Soap\Client $soapClient
    )
    {
        $this->logger = $loggerInterface;
        $this->_objectManager = $objectManager;
        $this->wm21wiModel = $this->_objectManager->create('Wm21w\Optima\Model\Wm21wi');
        $this->soapClient = $soapClient;
        $this->client = new  $this->soapClient($this->wm21wiModel->webServiceHost, ['soap_version' => SOAP_1_1]);
    }

    public function execute()
    {
        if ($stocks = $this->getStocks()) {

            foreach ($stocks as $a => $partial) {
                foreach ($partial as $stock) {

                    $mode = $this->wm21wiModel->checkIsProductExistInMagento($stock->Code);

                    if ($mode == 'update') {
                        $tmpProduct = $this->prepareTmpProductToSave($stock);
                        $this->wm21wiModel->updateProduct($tmpProduct);
                    }
                }
            }
        }
        return true;
    }

    private
    function prepareTmpProductToSave($stock)
    {
        $extAttr = [
//            "website_ids" => [
//                0
//            ],
            "stock_item" => [
                "qty" => $stock->Value,
                "is_in_stock" => ($stock->Value > 0) ? true : false
            ]
        ];

        return
            [
                'product' =>
                    [
                        'sku' => $stock->Code,
                        'extension_attributes' => $extAttr
                    ]
            ];
    }


    private
    function getStocks()
    {
        try {
            $results = $this->client->GetStockStates([
                'Authentication' => [
                    'Password' => $this->wm21wiModel->webServicePassword,
                    'Username' => $this->wm21wiModel->webServiceUsername,
                    'ShopId' => $this->wm21wiModel->webServiceShopId
                ],
                'IsDifferential' => $this->IsDifferential
            ]);
            if ($this->_checkRespond($results)) {
                $codes_ = array_chunk($results->StockStates->StockState, 100);
                return $codes_;
            } else {
                return false;
            }
        } catch (Exception $e) {
            $this->logger->debug($e->getMessage() . print_r($e, true));
        }
    }

    private
    function _checkRespond($results)
    {
        if (!$results->ErrorMessage
            && $results->LoginResult == 'OK'
        ) {
            return true;
        } else {
            return false;
        }
    }
}