<?php

namespace Wm21w\Optima\Model\ResourceModel;

class Wm21wi extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('wm21wi', 'wm21wi_id');
    }
}

?>