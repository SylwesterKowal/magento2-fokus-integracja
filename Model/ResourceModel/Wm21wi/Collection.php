<?php

namespace Wm21w\Optima\Model\ResourceModel\Wm21wi;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Wm21w\Optima\Model\Wm21wi', 'Wm21w\Optima\Model\ResourceModel\Wm21wi');
        $this->_map['fields']['page_id'] = 'main_table.page_id';
    }

}
?>