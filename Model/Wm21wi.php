<?php

namespace Wm21w\Optima\Model;

class Wm21wi extends \Magento\Framework\Model\AbstractModel
{
    protected $_requestURLcategories = "rest/V1/categories";
    protected $_requestURLproducts = "rest/V1/products";
    protected $_requestURLattribiuts = "rest/V1/products/attributes";
    protected $_requestURLattribiutsTypes = "rest/V1/products/attributes/types";
    public $accessToken;
    public $filterManager;
    protected $_tablePrefix = '';
    public $TableName = 'wm21wi';
    public $TableCategoriesName = 'catalog_category_flat_store_1';
    public $storeBaseUrl = 'http://sklepmagento1.hostland.pl/';
    public $webServiceHost = 'http://192.168.88.200:47000/Service.svc?wsdl';
    public $webServicePassword = 'esklep12';
    public $webServiceUsername = 'SYNC';
    public $webServiceShopId = 2;
    public $directory;
    public $_dbi;
    public $tmpStstusEmpty = 0;
    public $tmpStstusRedy = 1;
    public $tmpStstusDone = 2;
    public $tmpStstusErr = 8;
    public $tmpStstusArch = 9;
    public $tmpStstusProgress = 7;
    public $tmpStstusProgressData = 6;

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Wm21w\Optima\Model\ResourceModel\Wm21wi');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $this->filterManager = $objectManager->get('Magento\Framework\Filter\FilterManager');
        $this->_dbi = $resource->getConnection();
        $this->TableName = $this->_applyTablePrefix("PFX_" . $this->TableName);
        $this->accessToken = $this->getAccessToken();
        $this->directory = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
    }


    /**
     * Aktualizacja produktu w Magento poprzez REST
     * @param $productData
     * @param $adminToken
     */
    public function updateProduct($product)
    {
        $productData = ['product' => $product['product'], "saveOptions" => true];

        if ($response_ = $this->restRequestCurl($this->storeBaseUrl . $this->_requestURLproducts . '/' . $product['product']['sku'], $productData, 'PUT')) {
            $response = json_decode($response_);
            if (!isset($response->id)) {
                file_put_contents($this->getDirectory('var') . '/tmp/___' . $product['product']['sku'] . '_product.txt', print_r($response, true));
            } else {
                // jeśli brak zdjęć - uaktualnij
                if ($this->checkIsProductGalleryIsEmpty($product['product']['sku']) && isset($product['images'])) {
                    $this->addMedia($product['product']['sku'], $product['product']['name'], $product['images']);
                }
            }
        }
    }

    /**
     * Dodaanie nowego produktu w Magento poprzez REST
     * @param $productData
     * @param $adminToken
     */
    public function addProduct($product)
    {
        $productData = ['product' => $product['product'], "saveOptions" => true];

        if ($response_ = $this->restRequestCurl($this->storeBaseUrl . $this->_requestURLproducts, $productData, 'POST')) {
            $response = json_decode($response_);
            if (!isset($response->id)) {
                file_put_contents($this->getDirectory('var') . '/tmp/___' . $product['product']['sku'] . '_product.txt', print_r($response, true));
            } else {
                $this->addMedia($product['product']['sku'], $product['product']['name'], $product['images']);
            }
        }
    }

    public function getProduct($sku)
    {

        if ($response_ = $this->restRequestCurl($this->storeBaseUrl . $this->_requestURLproducts . '/' . $sku, false, 'GET')) {
            $response = json_decode($response_, true);

            if (!isset($response['id'])) {
                file_put_contents($this->getDirectory('var') . '/tmp/___get_err__' . $sku . 'product.txt', print_r($response, true));
                return false;
            }
            return $response;
        }
    }

    public function getAttribiutOptions($attCode)
    {
        return $this->restRequestCurl($this->storeBaseUrl . $this->_requestURLattribiuts . '/' . $attCode . '/options', false, 'GET');
    }

    public function setAttribiutOptions($attCode, $data_option)
    {
//        $data = json_encode(["option" => $data_option]);
        $attCode = $this->filterManager->translitUrl($attCode);

        $data = ["option" => [
            "label" => $data_option,
//            "sortOrder" => 100,
            "isDefault" => false,
            "storeLabels" => [
                ["storeId" => 1, "label" => $data_option]
            ]]];
        return $this->restRequestCurl($this->storeBaseUrl . $this->_requestURLattribiuts . '/' . $attCode . '/options', $data, 'POST');
    }

    public function setAttribiut($attCode, $attrName, $data_option, $type)
    {
        $attCode = $this->filterManager->translitUrl($attCode);

        $data = [
            "attribute" => [
                "is_wysiwyg_enabled" => false,
                "is_html_allowed_on_front" => false,
                "used_for_sort_by" => false,
                "is_filterable" => false,
                "is_filterable_in_search" => false,
                "is_used_in_grid" => false,
                "is_visible_in_grid" => false,
                "is_filterable_in_grid" => false,
                "position" => 0,
                "apply_to" => ["simple", "configurable", "virtual", "bundle", "downloadable"],
                "is_searchable" => false,
                "is_visible_in_advanced_search" => false,
                "is_comparable" => false,
                "is_used_for_promo_rules" => false,
                "is_visible_on_front" => false,
                "used_in_product_listing" => true,
                "is_visible" => true,
                "scope" => "global",
                "extension_attributes" => [],
                "attribute_code" => $attCode,
                "frontend_input" => $type,
                "is_required" => false,

                "is_user_defined" => true,
                "default_frontend_label" => $attrName,
                "note" => "",
//                "backend_type" => "int",
//                "backend_model" => "Magento\Eav\Model\Entity\Attribute\Source\Table",
//                "source_model" => "Magento\Eav\Model\Entity\Attribute\Source\Table",
                "is_unique" => false,
                "frontend_class" => "",
                "validation_rules" => [],
            ]
        ];

        switch ($type) {
            case 'select':
                $data['attribute']['custom_attributes'] = [

                    "attribute_code" => $attCode,
                    "value" => ""

                ];
                break;
        }

        return $this->restRequestCurl($this->storeBaseUrl . $this->_requestURLattribiuts, $data, 'POST');
    }


    public function getProductAttrTypes()
    {
        return $this->restRequestCurl($this->storeBaseUrl . $this->_requestURLattribiutsTypes, false, 'GET');
    }

    public function addMedia($sku, $product_name, $gallery)
    {
        try {
            $data["entry"] = [];
            if (!empty($gallery)) {
                $gallery = json_decode($gallery, true);
                if (is_array($gallery)) {


                    $directory = $this->getDirectory('var') . '/tmp';

                    foreach ($gallery as $imagename) {
                        $file = $directory . '/' . $imagename;
                        if (file_exists($file)) {
                            $image = file_get_contents($file);

                            $data["entry"] = [
                                "media_type" => "image",
                                "label" => $product_name,
                                "position" => 0,
                                "disabled" => 0,
                                "types" => ["image", "small_image", "thumbnail"],
                                "content" => [
                                    "base64_encoded_data" => $image,
                                    "type" => "image/jpeg",
                                    "name" => $imagename
                                ]
                            ];
                            $respons = $this->restRequestCurl($this->storeBaseUrl . $this->_requestURLproducts . '/' . $sku . '/media', $data, 'POST');
                            if ((int)$respons > 0) {
                                @unlink($directory . '/' . $imagename);
                            } else {
//                        if (!empty($respons))
//                            file_put_contents($directory . '/___' . $imagename . '.media_respon.txt', print_r($respons, true));
                            }
                        }

                    };
                }
            }
        } catch (Exception $e) {

        }
    }

    public function addCategories($categories)
    {
        if (is_array($categories->ProductGroups->ProductGroup)) {
            foreach ($categories->ProductGroups->ProductGroup as $category) {
                if ((int)$category->ParentId >= 0) {
                    if ($category_id = $this->getCategoryIdByGroupId($category->GroupId)) {
                        #TODO update category
                    } else {
                        $parent_id = $this->getCategoryIdByGroupId($category->ParentId);
                        $data = [
                            "category" => [
                                "parent_id" => $parent_id,
                                "name" => $category->Name,
                                "is_active" => true,
                                //    "position": 0,
                                //    "level": 0,
                                //    "children": "string",
                                //    "created_at": "string",
                                //    "updated_at": "string",
                                //    "path": "string",
                                //    "available_sort_by": [
                                //                            "string"
                                //                        ],
                                "include_in_menu" => true,
                                //    "extension_attributes": {},
                                "custom_attributes" => [
                                    [
                                        "attribute_code" => "group_id",
                                        "value" => $category->GroupId

                                    ]
                                ]
                            ]
                        ];
                        $result = $this->restRequestCurl($this->storeBaseUrl . $this->_requestURLcategories, $data, 'POST');
                    }
                }
            }
        }
    }

// =======================================

    public function selectOneTmpProduct($sku)
    {
        $query = "SELECT * FROM {$this->TableName} WHERE code = :sku";
        $entity = $this->_dbi->fetchRow($query, array("sku" => $sku));
        if (empty($entity) == true) {
            return false;
        } else {
            return $entity;
        }
    }

    public function getDirectory($dir)
    {
        return $this->directory->getPath($dir);
    }

    private
    function restRequest($url, $productData = false, $requestType = 'POST')
    {
        $rMetoth = $this->setRequestMethod($requestType);
        $httpHeaders = new \Zend\Http\Headers();
        $httpHeaders->addHeaders([
            'Authorization' => 'Bearer ' . $this->accessToken,
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
        ]);

        $request = new \Zend\Http\Request();
        $request->setHeaders($httpHeaders);
        $request->setUri($url);
        $request->setMethod($rMetoth);

        if ($productData) {
            $params = new \Zend\Stdlib\Parameters($productData);
            $request->setQuery($params);
        }

        $client = new \Zend\Http\Client();
        $options = [
            'adapter' => 'Zend\Http\Client\Adapter\Curl',
            'curloptions' => [CURLOPT_FOLLOWLOCATION => true],
            'maxredirects' => 0,
            'timeout' => 30
        ];
        $client->setOptions($options);

        $response = $client->send($request);
        return $response;
    }


    public
    function restRequestCurl($url, $productData, $requestType)
    {
        $ch = curl_init($url);

        $curlOptions = [
            CURLOPT_CUSTOMREQUEST => $requestType,
            CURLOPT_RETURNTRANSFER => true,

            CURLOPT_HTTPHEADER => array("Content-Type: application/json", "Authorization: Bearer " . $this->accessToken)
        ];
        if ($productData) {
            $curlOptions[CURLOPT_POSTFIELDS] = json_encode($productData);
//            file_put_contents('____product_data.txt', print_r($curlOptions[CURLOPT_POSTFIELDS], true));
        }

        curl_setopt_array($ch, $curlOptions);

        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }


    public
    function getCategoryIdByGroupId($group_id)
    {
        $query = "
			SELECT entity_id FROM {$this->TableCategoriesName} WHERE group_id = '{$group_id}'		
		";
        if ($result = $this->_dbi->fetchOne($query)) {
            // Jeśli wszystkie rekordy są równe 2 wykonaj kolejne działanie
            return $result;
        } else {
            // Jeśli sa rekordy rózna od 2 poczekaj na zakończenie poprzedniego zadania
            return false;
        }
    }


    public
    function checkIsProductExistInMagento($sku, $return_entity = false)
    {

        $query = "
				SELECT  cpe.entity_id 
				FROM PFX_catalog_product_entity AS cpe 
				WHERE cpe.sku = :sku
			";
        $query = $this->_applyTablePrefix($query);
        $entity = $this->_dbi->fetchOne($query, array("sku" => $sku));
        if (!$return_entity) {
            if (empty($entity) == true) {
                return 'add';
            } else {
                return 'update';
            }
        } else {
            return $entity;
        }
    }

    public function checkIsProductGalleryIsEmpty($sku)
    {
        $query = "SELECT a.entity_id FROM `PFX_catalog_product_entity` AS a 
                  LEFT JOIN `PFX_catalog_product_entity_media_gallery_value_to_entity` AS b ON a.entity_id = b.entity_id
                  WHERE a.sku = :sku AND b.value_id IS NULL";
        $query = $this->_applyTablePrefix($query);
        return $this->_dbi->fetchOne($query, array("sku" => $sku));
    }

    public
    function checkIsAttributeExistInMagento($attr_code)
    {
        $query = "
				SELECT ea.attribute_id, ea.attribute_code 
				FROM PFX_eav_attribute AS ea 
				WHERE ea.attribute_code = :attribute_code
			";
        $query = $this->_applyTablePrefix($query);
        $entity = $this->_dbi->fetchRow($query, array("attribute_code" => $attr_code));
        if (empty($entity) == true) {
            return false;
        } else {
            return true;
        }
    }

    public function getAttribiutValue($attrName, $sku, $tabel = 'catalog_product_entity_int')
    {
        $attrId = $this->_getAttributeId($attrName);
        $entity_id = $this->_getIdFromSku($sku);

        $query = "SELECT value FROM PFX_" . $tabel . " WHERE attribute_id = :attribute_id AND entity_id = :entity_id";
        $query = $this->_applyTablePrefix($query);
        return $this->_dbi->fetchOne($query, array("attribute_id" => $attrId, 'entity_id' => $entity_id));
    }

    public function _getAttributeId($attribute_code = 'price')
    {
        $query = "SELECT ea.attribute_id FROM PFX_eav_attribute AS ea WHERE ea.entity_type_id = :entity_type_id AND ea.attribute_code = :attribute_code";
        $query = $this->_applyTablePrefix($query);
        $entity_type_id = $this->_getEntityTypeId();
        return $this->_dbi->fetchOne($query, array("entity_type_id" => $entity_type_id, 'attribute_code' => $attribute_code));
    }

    private function _getEntityTypeId($entity_type_code = 'catalog_product')
    {
        $query = "SELECT entity_type_id FROM PFX_eav_entity_type WHERE entity_type_code = :entity_type_code";
        $query = $this->_applyTablePrefix($query);
        return $this->_dbi->fetchOne($query, array("entity_type_code" => $entity_type_code));
    }

    public function _getIdFromSku($sku)
    {
        $query = "SELECT entity_id FROM PFX_catalog_product_entity WHERE sku = :sku";
        $query = $this->_applyTablePrefix($query);
        return $this->_dbi->fetchOne($query, array("sku" => $sku));
    }

    public
    function getAttributOptionId($attrValue, $allAttribiutOptions)
    {
        $selectedOptionID = null;

        if (!empty($allAttribiutOptions)) {
            $allAttribiutOptions = json_decode($allAttribiutOptions, true);
            foreach ($allAttribiutOptions as $option) {
                if (isset($option['label'])) {
                    if (strtolower($option['label']) == strtolower($attrValue)) {
                        $selectedOptionID = $option['value'];
                    }
                }
            }
        }
        return $selectedOptionID;
    }

    public
    function _applyTablePrefix($query)
    {
        return str_replace('PFX_', $this->_tablePrefix, $query);
    }


    public
    function getAccessToken()
    {
        $query = "SELECT access_token FROM wm21wcreditials";
        $accessToken = $this->_dbi->fetchRow($query);
        return $accessToken['access_token'];
    }


    public
    function setDone($sku)
    {
        $query = "UPDATE  {$this->TableName} SET 
          `done` = " . $this->tmpStstusDone . ", 
          `executed` = '" . date('Y-m-d H:i:s') . "' WHERE code = '{$sku}'";
        $this->_executeQuery($query);
    }

    public
    function _executeQuery($query)
    {
        try {
            $stmt = $this->_dbi->prepare($query);
            $result = $stmt->execute();

            return $result;
        } catch (Exception $e) {
            return false;
        }
    }


    private
    function setrequestMethod($requestType)
    {
        switch ($requestType) {
            case 'POST':
                $rMetoth = \Zend\Http\Request::METHOD_POST;
                break;
            case 'GET':
                $rMetoth = \Zend\Http\Request::METHOD_GET;
                break;
            default:
                $rMetoth = \Zend\Http\Request::METHOD_POST;
                break;
        }
        return $rMetoth;
    }

    /**
     * Sprawdza czy wszystkie rekordy w tabeli wm21wi są przetworzone do magento
     * @return bool
     */
    public
    function checkIsAllDone()
    {
        $query = "
			SELECT COUNT(*) AS done FROM {$this->TableName} WHERE done NOT IN ({$this->tmpStstusDone},{$this->tmpStstusArch},{$this->tmpStstusErr})
		";
        if (!$result = $this->_dbi->fetchOne($query)) {
            // Jeśli wszystkie rekordy są równe 2 wykonaj kolejne działanie
            return true;
        } else {
            // Jeśli sa rekordy rózna od 2 poczekaj na zakończenie poprzedniego zadania
            return false;
        }
    }

    /**
     * Sprawdza czy są produkty gpotowe do przetwarzania
     * @return bool
     */
    public
    function checkIsProductsRedy()
    {
        $query = "
			SELECT COUNT(*) AS done FROM {$this->TableName} WHERE done = {$this->tmpStstusRedy}
		";
        if ($result = $this->_dbi->fetchOne($query)) {
            // Jeśli wszystkie rekordy są równe 2 wykonaj kolejne działanie
            return true;
        } else {
            // Jeśli sa rekordy rózna od 2 poczekaj na zakończenie poprzedniego zadania
            return false;
        }
    }
}

?>