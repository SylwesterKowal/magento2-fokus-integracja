<?php

namespace Wm21w\Optima\Observer;

class Customer implements \Magento\Framework\Event\ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        //$order= $observer->getData('order');
        //$order->doSomething();

        $customer = $observer->getEvent()->getCustomer();

        /**
         * TODO: Add some type of serialization which filters the
         * actual fields that get returned from the object. Returning
         * this raw data is dangerous and can expose sensitive data.
         *
         * Ideally this representation of the object will match that
         * of the json rest api. Maybe we can tap into that serializer?
         */
        $customer_ = [
            'customer' => $customer->getData()
        ];

        file_put_contents('_customer.txt', print_r($customer_, true));

        return $this;
    }
}