<?php

namespace Wm21w\Optima\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\DB\Adapter\AdapterInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.0') < 0){

		$installer->run('create table wm21wi(wm21wi_id int not null auto_increment, code varchar(30), brand varchar(30), ean varchar(30), name varchar(255), description varchar(255), unit varchar(5), group_id int, product_attr_json text, product_set_json text, product_image_json text, parent_product_code varchar(255), sale_price decimal(12,2), regular_price decimal(12,2), retail_price decimal(12,2), promo_price decimal(12,2), dealers_price decimal(12,2), tax decimal(12,2), currency varchar(3), primary key(wm21wi_id))');
//        $installer->run('insert into wm21wi values(null,\'wm21wi 1\')');
//        $installer->run('insert into wm21wi values(null,\'wm21wi 2\')');


		//demo 
//$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
//$scopeConfig = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface');
//demo 

		}

        $installer->endSetup();

    }
}